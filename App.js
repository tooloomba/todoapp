/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import store from './store'
import {Provider} from 'react-redux';
import { Router, Scene } from 'react-native-router-flux'
import Todos from './src/components/todos'
import Todo from './src/components/todo'
import Home from './src/components/home'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


const TabIcon = ({ selected, title }) => {
  return(
    <Text style={{color: selected ? 'red' :'black'}}> {title} </Text>
  )
}
type Props = {};
class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
      <Router>
      
      
      <Scene key="root">
      
        <Scene hideNavBar key="tabbar" tabs tabBarStyle={{ backgroundColor: '#FFFFFF'}}>

          <Scene key="TODO" title="TODO" icon={TabIcon}>
            <Scene key="todos" component={Todos} title="Todos" initial></Scene>
            <Scene key="todo" component={Todo} title="Todo"></Scene>
          </Scene>

          <Scene key="HomeTab" title="HOME" icon={TabIcon}>
            <Scene key="home" component={Home} title="Home"></Scene>
          </Scene>


        </Scene>

      </Scene>
      
      </Router>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    backgroundColor: '#9966ff'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default App;
