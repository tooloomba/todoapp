
import todos from './todos'
import { fork } from 'redux-saga/effects'

export default function* rootSaga () {
    yield [
        fork(todos)
    ];
}
