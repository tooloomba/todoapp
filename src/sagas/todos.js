import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';


import { FETCH_TODOS, FETCH_TODOS_SUCCESS, FETCH_TODOS_FAILURE,
            FETCH_TODO, FETCH_TODO_SUCCESS, FETCH_TODO_FAILURE,
            CREATE_TODO, CREATE_TODO_FAILURE, CREATE_TODO_SUCCESS } 
    from '../actions/todos';

function* fetchTodos(action){
    try{
        //const data = yield call(FP, action.payload.id);
        yield put({type: FETCH_TODOS_SUCCESS, payload: [{"text":"Default todo"}]});
    }catch(e){
        yield put({type: FETCH_TODOS_FAILURE});
    }
}

function* createTodo(action){
    try{
        yield put({type: CREATE_TODO_SUCCESS, payload: action.payload});
    }catch(e){
        yield put({type: CREATE_TODO_FAILURE});
    }
}
/*
function* fetchPatients(action){
    try{
        const data = yield call(FPs);
        yield put({ type: FETCH_PATIENTS_SUCCESS, payload: data})
    }catch(e){
        yield put({ type: FETCH_PATIENTS_FAILURE })
    }
}

function* createPatient(action){
    try{
        const data = call(CP, action.payload.data);
        yield put({ type: CREATE_PATIENT_SUCCESS, payload: data});
    }catch(e){
        yield put({ type: CREATE_PATIENT_FAILURE});
    }
}

function* deletePatient(action){
    try{
        const data = call(DP, action.payload.id);
        yield put({ type: DELETE_PATIENT_SUCCESS, payload: data});
    }catch(e){
        yield put({type: DELETE_PATIENT_FAILURE});
    }
}*/

export default function* patientsSaga(){
    yield takeEvery(FETCH_TODOS, fetchTodos);
    yield takeLatest(CREATE_TODO, createTodo);
}
