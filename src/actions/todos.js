export const FETCH_TODOS = "todo/FETCH_TODOS"
export const FETCH_TODOS_SUCCESS = "todo/FETCH_TODOS_SUCCESS"
export const FETCH_TODOS_FAILURE = "todo/FETCH_TODOS_FAILURE"
export const FETCH_TODO = "todo/FETCH_TODO"
export const FETCH_TODO_SUCCESS = "todo/FETCH_TODO_SUCCESS"
export const FETCH_TODO_FAILURE = "todo/FETCH_TODO_FAILURE"
export const CREATE_TODO = "todo/CREATE_TODO"
export const CREATE_TODO_SUCCESS = "todo/CREATE_TODO_SUCCESS"
export const CREATE_TODO_FAILURE = "todo/CREATE_TODO_FAILURE"

export const fetchTodos = () => ({
    type: FETCH_TODOS
})

export const fetchTodo = (name) => ({
    type: FETCH_TODOS,
    payload: name
})

export const createTodo = (text) => ({
    type: CREATE_TODO,
    payload: {text}
})
