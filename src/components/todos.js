import React, { Component } from 'react'
import { bindActionCreators } from 'redux';
import { fetchTodos } from '../actions/todos'
import { createTodo } from '../actions/todos'
import { Actions } from 'react-native-router-flux'
import { StyleSheet, View, Text, TouchableOpacity, TextInput, ImageBackground, Image } from 'react-native'
import { connect } from 'react-redux'
import { List, ListItem, Button } from 'react-native-elements'
import image from '../../src/todo-cloud-hero.png'


class todos extends Component {
    constructor(props){
        super(props);
        this.state ={
            list: [
                {
                  name: 'Amy Farha',
                  avatar_url: '',
                  subtitle: 'Vice President'
                },
                {
                  name: 'Chris Jackson',
                  avatar_url: '',
                  subtitle: 'Vice Chairman'
                },
              ],
            text: ''
        }
    }

    componentDidMount(){
        this.props.fetchTodos()
    }

    onChangeHandle = e => {
        this.setState({
            text: e
        })
    }

    createTodo = () => {
        this.props.createTodo(this.state.text)
    }

  render() {
      const { todos } = this.props;
    return (
        <View style={styles.container}>
        <TextInput onChangeText={this.onChangeHandle} style={styles.inputField} placeholder="Write something..." />
            <View onPress={Actions.todo} style={styles.listItem}>
            
                <List onPress={Actions.todo} containerStyle={{marginBottom: 20}}>
                {
                    todos.map((l) => (
                    <ListItem onPress={() => Actions.todo(l)} 
                        avatar={image}
                        key={l.text}
                        title={l.text}
                        
                    />
                    ))
                }
                </List>
            </View>
            <TouchableOpacity>
            <Button
                raised
                color={'black'}
                backgroundColor={'white'}
                title='Create' onPress={this.createTodo}/>
            </TouchableOpacity>

        </View>
      
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#66ccff',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    createButton:{
        flexDirection: 'row', 
        width: 200, 
        justifyContent: 'space-around'
    },
    listItem: {
        backgroundColor: '#66ccff',
    },
    inputField: {
        backgroundColor: 'white'
    }
  });
  

const mapStateToProps = state => ({
    todos: state.todos.list
})

const mapDispatchToProps = dispatch => bindActionCreators({ createTodo, fetchTodos }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(todos);
