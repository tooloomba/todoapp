import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

class home extends Component {


  render() {
    return (
        <View style={styles.container} >
            <Text >
                Welcome to my app
            </Text>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9966ff',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      backgroundColor: '#9966ff'
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => bindActionCreators({  }, dispatch);

export default connect()(home);
