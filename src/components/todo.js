import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { fetchTodos } from '../actions/todos'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';

class todo extends Component {
  render() {
    return (
        <View style={styles.container}>
            <Text style={styles.welcome}>
                {this.props.text}
            </Text>
      </View>
    )
  }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFFFFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });
  

const mapStateToProps = state => ({
    
})

const mapDispatchToProps = dispatch => bindActionCreators({ fetchTodos }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(todo);