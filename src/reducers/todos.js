import {FETCH_TODOS, FETCH_TODOS_SUCCESS, FETCH_TODOS_FAILURE, 
        FETCH_TODO, FETCH_TODO_SUCCESS, FETCH_TODO_FAILURE,
        CREATE_TODO, CREATE_TODO_FAILURE, CREATE_TODO_SUCCESS } 
    from '../actions/todos';

export default (state = {list: [], todo: null, created: false, loading: false, error: false}, action) => {
    switch(action.type){
        case FETCH_TODOS:
            return { ...state }
        case FETCH_TODOS_SUCCESS:
            return { ...state, list: action.payload}
        case FETCH_TODOS_FAILURE:
            return { ...state, error: true}
        case FETCH_TODO:
            return { ...state }
        case FETCH_TODO_SUCCESS:
            return { ...state, todo: action.payload.data}
        case FETCH_TODO_FAILURE:
            return { ...state, error: true}
        case CREATE_TODO:
            return { ...state }
        case CREATE_TODO_SUCCESS:
            return { ...state, list: [...state.list, action.payload]}
        case CREATE_TODO_FAILURE:
            return { ...state, error: true}
        default:
            return state;
    }

};
