import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import rootReducer from './src/reducers'
import rootSaga from './src/sagas/index'

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

middlewares.push(logger);

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

export default createStore(rootReducer, composedEnhancers);

sagaMiddleware.run(rootSaga);
